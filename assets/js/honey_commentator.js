var HoneyCommentator =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _honey_messenger = __webpack_require__(1);

var _honey_messenger2 = _interopRequireDefault(_honey_messenger);

var _comment_template = __webpack_require__(3);

var _comment_template2 = _interopRequireDefault(_comment_template);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var HoneyCommentator = function () {

  //------------- Конструктор
  function HoneyCommentator() {
    var _this = this;

    _classCallCheck(this, HoneyCommentator);

    this.honeyMessegner = new _honey_messenger2.default();

    this.form = $("form[name='commentForm']");
    if (this.form.length < 1) {
      throw new Error("Cannot find comment form");
    }

    $("label").on("click", function () {
      _this.submit();
    });

    this.form.on("submit", function () {
      _this.submit($(event.target));
      return false;
    });

    this.showComments();
  }

  //------------- Отправляет комментарий на сервер


  _createClass(HoneyCommentator, [{
    key: "submit",
    value: function submit() {
      var _this2 = this;

      var dataSet = {
        userName: $.trim($("input[name='userName']", this.form).val()),
        eMail: $.trim($("input[name='eMail']", this.form).val()),
        comment: $.trim($("textarea[name='comment']", this.form).val())
      };

      if (!dataSet.userName) {
        this.honeyMessegner.show("Отправка комментария", "Представьтесь, пожалуйста", function () {
          $("input[name='userName']", _this2.form).focus();
        });
        return false;
      }

      if (!dataSet.eMail) {
        this.honeyMessegner.show("Отправка комментария", "Наберите Ваш E-Mail.<br />Мы не будем спамить, честно-честно!", function () {
          $("input[name='eMail']", _this2.form).focus();
        });
        return false;
      }

      if (!this.validateEmail(dataSet.eMail)) {
        this.honeyMessegner.show("Отправка комментария", "Неправильный E-Mail", function () {
          $("input[name='eMail']", _this2.form).focus();
        });
        return false;
      }

      if (!dataSet.comment) {
        this.honeyMessegner.show("Отправка комментария", "Что жы Вы хотели сказать?<br />Наберите Ваш комментарий.", function () {
          $("textarea[name='comment']", _this2.form).focus();
        });
        return false;
      }

      if (dataSet.userName != dataSet.userName.replace(/(<([^>]+)>)/ig, "") || dataSet.eMail != dataSet.eMail.replace(/(<([^>]+)>)/ig, "") || dataSet.comment != dataSet.comment.replace(/(<([^>]+)>)/ig, "")) {
        this.honeyMessegner.show("Отправка комментария", "Увы, форматирование не поддерживается.<br />Уберите, пожалуйста, тэги.", function () {
          $("textarea[name='comment']", _this2.form).focus();
        });
        return false;
      }

      this.disable();

      $.ajax({
        url: "http://test.webbuild.ru/api/" + "put_comment.php",
        type: "post",
        dataType: "json",
        data: dataSet,

        success: function success(data, textStatus, XMLHttpRequest) {
          _this2.enable();

          if ((typeof data === "undefined" ? "undefined" : _typeof(data)) != "object" || data == null || data.code != "OK") {
            var message = (typeof data === "undefined" ? "undefined" : _typeof(data)) == "object" && data != null && typeof data.message == "string" && $.trim(data.message) ? $.trim(data.message) : "Не удалось отправить комментарий";
            var field = (typeof data === "undefined" ? "undefined" : _typeof(data)) == "object" && data != null && typeof data.field == "string" && $.trim(data.field) ? $.trim(data.field) : "";
            _this2.honeyMessegner.show("Отправка комментария", message, function () {
              if (field) {
                $("[name='" + field + "']", _this2.form).focus();
              }
            });
            return;
          }

          _this2.clear().showComments(true);
        },

        error: function error(XMLHttpRequest, textStatus, errorThrown) {
          _this2.enable();
          _this2.honeyMessegner.show("Отправка комментария", typeof errorThrown == "string" && $.trim(errorThrown) ? errorThrown : "Не удалось отправить комментарий");
        }
      });

      return this;
    }

    //------------- Проверяет email

  }, {
    key: "validateEmail",
    value: function validateEmail(value) {
      var rgxp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return rgxp.test(String(value).toLowerCase());
    }

    //------------- Очищает форму

  }, {
    key: "clear",
    value: function clear() {
      $("input, textarea", this.form).val("");
      return this;
    }

    //------------- Дизаблит форму

  }, {
    key: "disable",
    value: function disable() {
      $("input, textarea, button", this.form).prop("disabled", true);
      $("[type='submit']", this.form).addClass("wait");
      return this;
    }

    //------------- Энаблит форму

  }, {
    key: "enable",
    value: function enable() {
      $("input, textarea, button", this.form).prop("disabled", false);
      $("[type='submit']", this.form).removeClass("wait");
      return this;
    }

    //------------- Возвращает комментарии

  }, {
    key: "showComments",
    value: function showComments(scroll) {
      var _this3 = this;

      if (scroll) {

        var comments = $(".comment", list);
        if (comments.length > 0) {
          $([document.documentElement, document.body]).animate({ scrollTop: $($(".comment", list)[0]).offset().top + $($(".comment", list)[0]).height() }, 200);
        } else {
          $([document.documentElement, document.body]).animate({ scrollTop: $(".comments h2").offset().top }, 200);
        }
      }

      var list = $(".list");
      list.addClass("wait");

      $("<div></div>").addClass("circonf large").appendTo(list);

      $.ajax({
        url: "http://test.webbuild.ru/api/" + "get_comments.php",
        type: "post",
        dataType: "json",
        data: {},

        success: function success(data, textStatus, XMLHttpRequest) {
          list.empty().removeClass("wait");

          if ((typeof data === "undefined" ? "undefined" : _typeof(data)) != "object" || data == null || data.code != "OK") {
            var message = (typeof data === "undefined" ? "undefined" : _typeof(data)) == "object" && data != null && typeof data.message == "string" && $.trim(data.message) ? $.trim(data.message) : "Не удалось получить список комментариев";
            var field = (typeof data === "undefined" ? "undefined" : _typeof(data)) == "object" && data != null && typeof data.field == "string" && $.trim(data.field) ? $.trim(data.field) : "";
            _this3.honeyMessegner.show("", message, function () {
              if (field) {
                $("[name='" + field + "']", _this3.form).focus();
              }
            });
            return;
          }

          if (!$.isArray(data.comments) || data.comments.length < 1) {
            $(".comments h2").text("Комментариев пока нет");
            return;
          }

          var compiledTemplate = _.template(_comment_template2.default);

          var _iteratorNormalCompletion = true;
          var _didIteratorError = false;
          var _iteratorError = undefined;

          try {
            for (var _iterator = data.comments[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
              var comment = _step.value;

              var el = compiledTemplate(comment);
              $(el).appendTo(list);
            }
          } catch (err) {
            _didIteratorError = true;
            _iteratorError = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion && _iterator.return) {
                _iterator.return();
              }
            } finally {
              if (_didIteratorError) {
                throw _iteratorError;
              }
            }
          }
        },

        error: function error(XMLHttpRequest, textStatus, errorThrown) {
          list.empty().removeClass("wait");
          _this3.honeyMessegner.show("", typeof errorThrown == "string" && $.trim(errorThrown) ? errorThrown : "Не удалось получить список комментариев");
        }
      });

      return this;
    }
  }]);

  return HoneyCommentator;
}();

exports.default = HoneyCommentator;

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _template = __webpack_require__(2);

var _template2 = _interopRequireDefault(_template);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var HoneyMessenger = function () {
	function HoneyMessenger() {
		_classCallCheck(this, HoneyMessenger);
	}

	_createClass(HoneyMessenger, [{
		key: "show",
		value: function show(title, text, onClose) {

			if (!this.$el) {
				var compiledTemplate = _.template(_template2.default);
				var el = compiledTemplate();
				this.$el = $(el);
				this.$el.appendTo($("body"));
			}

			$(".modal-title", this.$el).html($.trim(text) ? title : "");
			$(".modal-body", this.$el).html($.trim(text) ? text : title);

			if (typeof onClose == "function") {
				this.$el.on("hidden.bs.modal", function () {
					onClose.call();
				});
			} else {
				this.$el.off("hidden.bs.modal");
			}

			this.$el.modal("show");
		}
	}]);

	return HoneyMessenger;
}();

exports.default = HoneyMessenger;

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = "<div id=\"honeyModal\" class=\"modal\" tabindex=\"-1\" role=\"dialog\">\r\n\t<div class=\"modal-dialog\" role=\"document\">\r\n\t\t<div class=\"modal-content\">\r\n\t\t\t<div class=\"modal-header\">\r\n\t\t\t\t<h5 class=\"modal-title\">Modal title</h5>\r\n\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>\r\n\t\t\t\t</button>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"modal-body\">\r\n\t\t\t\t<p>Modal body text goes here.</p>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"modal-footer\">\r\n\t\t\t\t<button type=\"button\" class=\"btn btn-ruby\" data-dismiss=\"modal\">Закрыть</button>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</div>"

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = "<div class=\"comment\" data-id=\"<%- id %>\">\r\n\t<div class=\"title\"><%- name %></div>\r\n\t<div class=\"email\"><a href=\"mailto:<%- email %>\"><%- email %></a></div>\r\n\t<div class=\"body\"><%- comment %></div>\r\n</div>\r\n"

/***/ })
/******/ ]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9uZXlfY29tbWVudGF0b3IuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgMzczZGJmMjg1MDU3ODMzNDgwMmIiLCJ3ZWJwYWNrOi8vL3NyYy9ub2RlX21vZHVsZXMvaG9uZXlfY29tbWVudGF0b3IvaW5kZXguanMiLCJ3ZWJwYWNrOi8vL3NyYy9ub2RlX21vZHVsZXMvaG9uZXlfbWVzc2VuZ2VyL2luZGV4LmpzIiwid2VicGFjazovLy8uL3NyYy9ub2RlX21vZHVsZXMvaG9uZXlfbWVzc2VuZ2VyL3RlbXBsYXRlLmh0bWwiLCJ3ZWJwYWNrOi8vLy4vc3JjL25vZGVfbW9kdWxlcy9ob25leV9jb21tZW50YXRvci9jb21tZW50X3RlbXBsYXRlLmh0bWwiXSwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4gXHRcdFx0XHRjb25maWd1cmFibGU6IGZhbHNlLFxuIFx0XHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcbiBcdFx0XHRcdGdldDogZ2V0dGVyXG4gXHRcdFx0fSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gMCk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgMzczZGJmMjg1MDU3ODMzNDgwMmIiLCJpbXBvcnQgSG9uZXlNZXNzZW5nZXIgZnJvbSBcIi4uL2hvbmV5X21lc3NlbmdlclwiO1xyXG5pbXBvcnQgQ29tbWVudFRlbXBsYXRlIGZyb20gXCIuL2NvbW1lbnRfdGVtcGxhdGUuaHRtbFwiO1xyXG5cclxuY2xhc3MgSG9uZXlDb21tZW50YXRvciB7XHJcblxyXG4gIC8vLS0tLS0tLS0tLS0tLSDQmtC+0L3RgdGC0YDRg9C60YLQvtGAXHJcbiAgY29uc3RydWN0b3IoKSB7XHJcbiAgICB0aGlzLmhvbmV5TWVzc2VnbmVyID0gbmV3IEhvbmV5TWVzc2VuZ2VyKCk7XHJcblxyXG4gICAgdGhpcy5mb3JtID0gJChcImZvcm1bbmFtZT0nY29tbWVudEZvcm0nXVwiKTtcclxuICAgIGlmICh0aGlzLmZvcm0ubGVuZ3RoIDwgMSkge1xyXG4gICAgICB0aHJvdyBuZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBjb21tZW50IGZvcm1cIik7XHJcbiAgICB9XHJcblxyXG4gICAgJChcImxhYmVsXCIpLm9uKFwiY2xpY2tcIiwgKCkgPT4geyB0aGlzLnN1Ym1pdCgpOyB9KTtcclxuXHJcbiAgICB0aGlzLmZvcm0ub24oXCJzdWJtaXRcIiwgKCkgPT4ge1xyXG4gICAgICB0aGlzLnN1Ym1pdCgkKGV2ZW50LnRhcmdldCkpO1xyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9KTtcclxuXHJcbiAgICB0aGlzLnNob3dDb21tZW50cygpO1xyXG4gIH1cclxuXHJcblxyXG4gIC8vLS0tLS0tLS0tLS0tLSDQntGC0L/RgNCw0LLQu9GP0LXRgiDQutC+0LzQvNC10L3RgtCw0YDQuNC5INC90LAg0YHQtdGA0LLQtdGAXHJcbiAgc3VibWl0KCkge1xyXG4gICAgbGV0IGRhdGFTZXQgPSB7XHJcbiAgICAgIHVzZXJOYW1lOiAkLnRyaW0oJChcImlucHV0W25hbWU9J3VzZXJOYW1lJ11cIiwgdGhpcy5mb3JtKS52YWwoKSksXHJcbiAgICAgIGVNYWlsOiAkLnRyaW0oJChcImlucHV0W25hbWU9J2VNYWlsJ11cIiwgdGhpcy5mb3JtKS52YWwoKSksXHJcbiAgICAgIGNvbW1lbnQ6ICQudHJpbSgkKFwidGV4dGFyZWFbbmFtZT0nY29tbWVudCddXCIsIHRoaXMuZm9ybSkudmFsKCkpXHJcbiAgICB9O1xyXG5cclxuICAgIGlmICghZGF0YVNldC51c2VyTmFtZSkge1xyXG4gICAgICB0aGlzLmhvbmV5TWVzc2VnbmVyLnNob3coXCLQntGC0L/RgNCw0LLQutCwINC60L7QvNC80LXQvdGC0LDRgNC40Y9cIiwgXCLQn9GA0LXQtNGB0YLQsNCy0YzRgtC10YHRjCwg0L/QvtC20LDQu9GD0LnRgdGC0LBcIiwgKCkgPT4geyAkKFwiaW5wdXRbbmFtZT0ndXNlck5hbWUnXVwiLCB0aGlzLmZvcm0pLmZvY3VzKCk7IH0pO1xyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKCFkYXRhU2V0LmVNYWlsKSB7XHJcbiAgICAgIHRoaXMuaG9uZXlNZXNzZWduZXIuc2hvdyhcItCe0YLQv9GA0LDQstC60LAg0LrQvtC80LzQtdC90YLQsNGA0LjRj1wiLCBcItCd0LDQsdC10YDQuNGC0LUg0JLQsNGIIEUtTWFpbC48YnIgLz7QnNGLINC90LUg0LHRg9C00LXQvCDRgdC/0LDQvNC40YLRjCwg0YfQtdGB0YLQvdC+LdGH0LXRgdGC0L3QviFcIiwgKCkgPT4geyAkKFwiaW5wdXRbbmFtZT0nZU1haWwnXVwiLCB0aGlzLmZvcm0pLmZvY3VzKCk7IH0pO1xyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKCF0aGlzLnZhbGlkYXRlRW1haWwoZGF0YVNldC5lTWFpbCkpIHtcclxuICAgICAgdGhpcy5ob25leU1lc3NlZ25lci5zaG93KFwi0J7RgtC/0YDQsNCy0LrQsCDQutC+0LzQvNC10L3RgtCw0YDQuNGPXCIsIFwi0J3QtdC/0YDQsNCy0LjQu9GM0L3Ri9C5IEUtTWFpbFwiLCAoKSA9PiB7ICQoXCJpbnB1dFtuYW1lPSdlTWFpbCddXCIsIHRoaXMuZm9ybSkuZm9jdXMoKTsgfSk7XHJcbiAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAoIWRhdGFTZXQuY29tbWVudCkge1xyXG4gICAgICB0aGlzLmhvbmV5TWVzc2VnbmVyLnNob3coXCLQntGC0L/RgNCw0LLQutCwINC60L7QvNC80LXQvdGC0LDRgNC40Y9cIiwgXCLQp9GC0L4g0LbRiyDQktGLINGF0L7RgtC10LvQuCDRgdC60LDQt9Cw0YLRjD88YnIgLz7QndCw0LHQtdGA0LjRgtC1INCS0LDRiCDQutC+0LzQvNC10L3RgtCw0YDQuNC5LlwiLCAoKSA9PiB7ICQoXCJ0ZXh0YXJlYVtuYW1lPSdjb21tZW50J11cIiwgdGhpcy5mb3JtKS5mb2N1cygpOyB9KTtcclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICgoZGF0YVNldC51c2VyTmFtZSAhPSBkYXRhU2V0LnVzZXJOYW1lLnJlcGxhY2UoLyg8KFtePl0rKT4pL2lnLFwiXCIpKSB8fCAoZGF0YVNldC5lTWFpbCAhPSBkYXRhU2V0LmVNYWlsLnJlcGxhY2UoLyg8KFtePl0rKT4pL2lnLFwiXCIpKSB8fCAoZGF0YVNldC5jb21tZW50ICE9IGRhdGFTZXQuY29tbWVudC5yZXBsYWNlKC8oPChbXj5dKyk+KS9pZyxcIlwiKSkpIHtcclxuICAgICAgdGhpcy5ob25leU1lc3NlZ25lci5zaG93KFwi0J7RgtC/0YDQsNCy0LrQsCDQutC+0LzQvNC10L3RgtCw0YDQuNGPXCIsIFwi0KPQstGLLCDRhNC+0YDQvNCw0YLQuNGA0L7QstCw0L3QuNC1INC90LUg0L/QvtC00LTQtdGA0LbQuNCy0LDQtdGC0YHRjy48YnIgLz7Qo9Cx0LXRgNC40YLQtSwg0L/QvtC20LDQu9GD0LnRgdGC0LAsINGC0Y3Qs9C4LlwiLCAoKSA9PiB7ICQoXCJ0ZXh0YXJlYVtuYW1lPSdjb21tZW50J11cIiwgdGhpcy5mb3JtKS5mb2N1cygpOyB9KTtcclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuZGlzYWJsZSgpO1xyXG5cclxuICAgICQuYWpheCh7XHJcbiAgICAgIHVybDogQVBJX1VSSSArIFwicHV0X2NvbW1lbnQucGhwXCIsXHJcbiAgICAgIHR5cGU6IFwicG9zdFwiLFxyXG4gICAgICBkYXRhVHlwZTogXCJqc29uXCIsXHJcbiAgICAgIGRhdGE6IGRhdGFTZXQsXHJcblxyXG4gICAgICBzdWNjZXNzOiAoZGF0YSwgdGV4dFN0YXR1cywgWE1MSHR0cFJlcXVlc3QpID0+IHtcclxuICAgICAgICB0aGlzLmVuYWJsZSgpO1xyXG5cclxuICAgICAgICBpZiAoKHR5cGVvZihkYXRhKSAhPSBcIm9iamVjdFwiKSB8fCAoZGF0YSA9PSBudWxsKSB8fCAoZGF0YS5jb2RlICE9IFwiT0tcIikpIHtcclxuICAgICAgICAgIGxldCBtZXNzYWdlID0gKCh0eXBlb2YoZGF0YSkgPT0gXCJvYmplY3RcIikgJiYgKGRhdGEgIT0gbnVsbCkgJiYgKHR5cGVvZihkYXRhLm1lc3NhZ2UpID09IFwic3RyaW5nXCIpICYmICQudHJpbShkYXRhLm1lc3NhZ2UpKSA/ICQudHJpbShkYXRhLm1lc3NhZ2UpIDogXCLQndC1INGD0LTQsNC70L7RgdGMINC+0YLQv9GA0LDQstC40YLRjCDQutC+0LzQvNC10L3RgtCw0YDQuNC5XCI7XHJcbiAgICAgICAgICBsZXQgZmllbGQgPSAoKHR5cGVvZihkYXRhKSA9PSBcIm9iamVjdFwiKSAmJiAoZGF0YSAhPSBudWxsKSAmJiAodHlwZW9mKGRhdGEuZmllbGQpID09IFwic3RyaW5nXCIpICYmICQudHJpbShkYXRhLmZpZWxkKSkgPyAkLnRyaW0oZGF0YS5maWVsZCkgOiBcIlwiO1xyXG4gICAgICAgICAgdGhpcy5ob25leU1lc3NlZ25lci5zaG93KFxyXG4gICAgICAgICAgICBcItCe0YLQv9GA0LDQstC60LAg0LrQvtC80LzQtdC90YLQsNGA0LjRj1wiLFxyXG4gICAgICAgICAgICBtZXNzYWdlLFxyXG4gICAgICAgICAgICAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgaWYgKGZpZWxkKSB7ICQoXCJbbmFtZT0nXCIgKyBmaWVsZCArIFwiJ11cIiwgdGhpcy5mb3JtKS5mb2N1cygpOyB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICk7XHJcbiAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLmNsZWFyKCkuc2hvd0NvbW1lbnRzKHRydWUpO1xyXG4gICAgICB9LFxyXG5cclxuICAgICAgZXJyb3I6IChYTUxIdHRwUmVxdWVzdCwgdGV4dFN0YXR1cywgZXJyb3JUaHJvd24pID0+IHtcclxuICAgICAgICB0aGlzLmVuYWJsZSgpO1xyXG4gICAgICAgIHRoaXMuaG9uZXlNZXNzZWduZXIuc2hvdyhcItCe0YLQv9GA0LDQstC60LAg0LrQvtC80LzQtdC90YLQsNGA0LjRj1wiLCAoKHR5cGVvZihlcnJvclRocm93bikgPT0gXCJzdHJpbmdcIikgJiYgJC50cmltKGVycm9yVGhyb3duKSkgPyBlcnJvclRocm93biA6IFwi0J3QtSDRg9C00LDQu9C+0YHRjCDQvtGC0L/RgNCw0LLQuNGC0Ywg0LrQvtC80LzQtdC90YLQsNGA0LjQuVwiKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgXHJcbiAgICByZXR1cm4gdGhpcztcclxuICB9XHJcblxyXG5cclxuICAvLy0tLS0tLS0tLS0tLS0g0J/RgNC+0LLQtdGA0Y/QtdGCIGVtYWlsXHJcbiAgdmFsaWRhdGVFbWFpbCh2YWx1ZSkge1xyXG4gICAgbGV0IHJneHAgPSAvXigoW148PigpXFxbXFxdXFxcXC4sOzpcXHNAXCJdKyhcXC5bXjw+KClcXFtcXF1cXFxcLiw7Olxcc0BcIl0rKSopfChcIi4rXCIpKUAoKFxcW1swLTldezEsM31cXC5bMC05XXsxLDN9XFwuWzAtOV17MSwzfVxcLlswLTldezEsM31cXF0pfCgoW2EtekEtWlxcLTAtOV0rXFwuKStbYS16QS1aXXsyLH0pKSQvO1xyXG4gICAgcmV0dXJuIHJneHAudGVzdChTdHJpbmcodmFsdWUpLnRvTG93ZXJDYXNlKCkpO1xyXG4gIH1cclxuXHJcblxyXG4gIC8vLS0tLS0tLS0tLS0tLSDQntGH0LjRidCw0LXRgiDRhNC+0YDQvNGDXHJcbiAgY2xlYXIoKSB7XHJcbiAgICAkKFwiaW5wdXQsIHRleHRhcmVhXCIsIHRoaXMuZm9ybSkudmFsKFwiXCIpO1xyXG4gICAgcmV0dXJuIHRoaXM7XHJcbiAgfVxyXG5cclxuXHJcbiAgLy8tLS0tLS0tLS0tLS0tINCU0LjQt9Cw0LHQu9C40YIg0YTQvtGA0LzRg1xyXG4gIGRpc2FibGUoKSB7XHJcbiAgICAkKFwiaW5wdXQsIHRleHRhcmVhLCBidXR0b25cIiwgdGhpcy5mb3JtKS5wcm9wKFwiZGlzYWJsZWRcIiwgdHJ1ZSk7XHJcbiAgICAkKFwiW3R5cGU9J3N1Ym1pdCddXCIsIHRoaXMuZm9ybSkuYWRkQ2xhc3MoXCJ3YWl0XCIpO1xyXG4gICAgcmV0dXJuIHRoaXM7XHJcbiAgfVxyXG5cclxuXHJcbiAgLy8tLS0tLS0tLS0tLS0tINCt0L3QsNCx0LvQuNGCINGE0L7RgNC80YNcclxuICBlbmFibGUoKSB7XHJcbiAgICAkKFwiaW5wdXQsIHRleHRhcmVhLCBidXR0b25cIiwgdGhpcy5mb3JtKS5wcm9wKFwiZGlzYWJsZWRcIiwgZmFsc2UpO1xyXG4gICAgJChcIlt0eXBlPSdzdWJtaXQnXVwiLCB0aGlzLmZvcm0pLnJlbW92ZUNsYXNzKFwid2FpdFwiKTtcclxuICAgIHJldHVybiB0aGlzO1xyXG4gIH1cclxuXHJcblxyXG4gIC8vLS0tLS0tLS0tLS0tLSDQktC+0LfQstGA0LDRidCw0LXRgiDQutC+0LzQvNC10L3RgtCw0YDQuNC4XHJcbiAgc2hvd0NvbW1lbnRzKHNjcm9sbCkge1xyXG4gICAgaWYgKHNjcm9sbCkge1xyXG5cclxuICAgICAgbGV0IGNvbW1lbnRzID0gJChcIi5jb21tZW50XCIsIGxpc3QpO1xyXG4gICAgICBpZiAoY29tbWVudHMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAkKFtkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQsIGRvY3VtZW50LmJvZHldKS5hbmltYXRlKFxyXG4gICAgICAgICAgICB7c2Nyb2xsVG9wOiAkKCQoXCIuY29tbWVudFwiLCBsaXN0KVswXSkub2Zmc2V0KCkudG9wICsgJCgkKFwiLmNvbW1lbnRcIiwgbGlzdClbMF0pLmhlaWdodCgpfSxcclxuICAgICAgICAgICAgMjAwXHJcbiAgICAgICAgICApO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgICQoW2RvY3VtZW50LmRvY3VtZW50RWxlbWVudCwgZG9jdW1lbnQuYm9keV0pLmFuaW1hdGUoXHJcbiAgICAgICAgICB7c2Nyb2xsVG9wOiAkKFwiLmNvbW1lbnRzIGgyXCIpLm9mZnNldCgpLnRvcH0sXHJcbiAgICAgICAgICAyMDBcclxuICAgICAgICApO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgbGV0IGxpc3QgPSAkKFwiLmxpc3RcIik7XHJcbiAgICBsaXN0LmFkZENsYXNzKFwid2FpdFwiKTtcclxuXHJcbiAgICAkKFwiPGRpdj48L2Rpdj5cIikuYWRkQ2xhc3MoXCJjaXJjb25mIGxhcmdlXCIpLmFwcGVuZFRvKGxpc3QpO1xyXG5cclxuICAgICQuYWpheCh7XHJcbiAgICAgIHVybDogQVBJX1VSSSArIFwiZ2V0X2NvbW1lbnRzLnBocFwiLFxyXG4gICAgICB0eXBlOiBcInBvc3RcIixcclxuICAgICAgZGF0YVR5cGU6IFwianNvblwiLFxyXG4gICAgICBkYXRhOiB7fSxcclxuXHJcbiAgICAgIHN1Y2Nlc3M6IChkYXRhLCB0ZXh0U3RhdHVzLCBYTUxIdHRwUmVxdWVzdCkgPT4ge1xyXG4gICAgICAgIGxpc3QuZW1wdHkoKS5yZW1vdmVDbGFzcyhcIndhaXRcIik7XHJcblxyXG4gICAgICAgIGlmICgodHlwZW9mKGRhdGEpICE9IFwib2JqZWN0XCIpIHx8IChkYXRhID09IG51bGwpIHx8IChkYXRhLmNvZGUgIT0gXCJPS1wiKSkge1xyXG4gICAgICAgICAgbGV0IG1lc3NhZ2UgPSAoKHR5cGVvZihkYXRhKSA9PSBcIm9iamVjdFwiKSAmJiAoZGF0YSAhPSBudWxsKSAmJiAodHlwZW9mKGRhdGEubWVzc2FnZSkgPT0gXCJzdHJpbmdcIikgJiYgJC50cmltKGRhdGEubWVzc2FnZSkpID8gJC50cmltKGRhdGEubWVzc2FnZSkgOiBcItCd0LUg0YPQtNCw0LvQvtGB0Ywg0L/QvtC70YPRh9C40YLRjCDRgdC/0LjRgdC+0Log0LrQvtC80LzQtdC90YLQsNGA0LjQtdCyXCI7XHJcbiAgICAgICAgICBsZXQgZmllbGQgPSAoKHR5cGVvZihkYXRhKSA9PSBcIm9iamVjdFwiKSAmJiAoZGF0YSAhPSBudWxsKSAmJiAodHlwZW9mKGRhdGEuZmllbGQpID09IFwic3RyaW5nXCIpICYmICQudHJpbShkYXRhLmZpZWxkKSkgPyAkLnRyaW0oZGF0YS5maWVsZCkgOiBcIlwiO1xyXG4gICAgICAgICAgdGhpcy5ob25leU1lc3NlZ25lci5zaG93KFxyXG4gICAgICAgICAgICBcIlwiLFxyXG4gICAgICAgICAgICBtZXNzYWdlLFxyXG4gICAgICAgICAgICAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgaWYgKGZpZWxkKSB7ICQoXCJbbmFtZT0nXCIgKyBmaWVsZCArIFwiJ11cIiwgdGhpcy5mb3JtKS5mb2N1cygpOyB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICk7XHJcbiAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoISQuaXNBcnJheShkYXRhLmNvbW1lbnRzKSB8fCAoZGF0YS5jb21tZW50cy5sZW5ndGggPCAxKSkge1xyXG4gICAgICAgICAgJChcIi5jb21tZW50cyBoMlwiKS50ZXh0KFwi0JrQvtC80LzQtdC90YLQsNGA0LjQtdCyINC/0L7QutCwINC90LXRglwiKTtcclxuICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBjb21waWxlZFRlbXBsYXRlID0gXy50ZW1wbGF0ZShDb21tZW50VGVtcGxhdGUpO1xyXG5cclxuICAgICAgICBmb3IgKGxldCBjb21tZW50IG9mIGRhdGEuY29tbWVudHMpIHtcclxuICAgICAgICAgIGxldCBlbCA9IGNvbXBpbGVkVGVtcGxhdGUoY29tbWVudCk7XHJcbiAgICAgICAgICAkKGVsKS5hcHBlbmRUbyhsaXN0KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICB9LFxyXG5cclxuICAgICAgZXJyb3I6IChYTUxIdHRwUmVxdWVzdCwgdGV4dFN0YXR1cywgZXJyb3JUaHJvd24pID0+IHtcclxuICAgICAgICBsaXN0LmVtcHR5KCkucmVtb3ZlQ2xhc3MoXCJ3YWl0XCIpO1xyXG4gICAgICAgIHRoaXMuaG9uZXlNZXNzZWduZXIuc2hvdyhcIlwiLCAoKHR5cGVvZihlcnJvclRocm93bikgPT0gXCJzdHJpbmdcIikgJiYgJC50cmltKGVycm9yVGhyb3duKSkgPyBlcnJvclRocm93biA6IFwi0J3QtSDRg9C00LDQu9C+0YHRjCDQv9C+0LvRg9GH0LjRgtGMINGB0L/QuNGB0L7QuiDQutC+0LzQvNC10L3RgtCw0YDQuNC10LJcIik7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIFxyXG4gICAgcmV0dXJuIHRoaXM7XHJcbiAgfVxyXG5cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgSG9uZXlDb21tZW50YXRvcjtcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHNyYy9ub2RlX21vZHVsZXMvaG9uZXlfY29tbWVudGF0b3IvaW5kZXguanMiLCJpbXBvcnQgVGVtcGxhdGUgZnJvbSBcIi4vdGVtcGxhdGUuaHRtbFwiO1xyXG5cclxuY2xhc3MgSG9uZXlNZXNzZW5nZXIge1xyXG5cclxuXHRzaG93KHRpdGxlLCB0ZXh0LCBvbkNsb3NlKSB7XHJcblxyXG5cdFx0aWYgKCF0aGlzLiRlbCkge1xyXG5cdFx0XHRsZXQgY29tcGlsZWRUZW1wbGF0ZSA9IF8udGVtcGxhdGUoVGVtcGxhdGUpO1xyXG5cdFx0XHRsZXQgZWwgPSBjb21waWxlZFRlbXBsYXRlKCk7XHJcblx0XHRcdHRoaXMuJGVsID0gJChlbCk7XHJcblx0XHRcdHRoaXMuJGVsLmFwcGVuZFRvKCQoXCJib2R5XCIpKTtcclxuXHRcdH1cclxuXHJcblx0XHQkKFwiLm1vZGFsLXRpdGxlXCIsIHRoaXMuJGVsKS5odG1sKCQudHJpbSh0ZXh0KSA/IHRpdGxlIDogXCJcIik7XHJcblx0XHQkKFwiLm1vZGFsLWJvZHlcIiwgdGhpcy4kZWwpLmh0bWwoJC50cmltKHRleHQpID8gdGV4dCA6IHRpdGxlKTtcclxuXHJcblx0XHRpZiAodHlwZW9mKG9uQ2xvc2UpID09IFwiZnVuY3Rpb25cIikge1xyXG5cdFx0XHR0aGlzLiRlbC5vbihcImhpZGRlbi5icy5tb2RhbFwiLCBmdW5jdGlvbiAoKSB7XHJcblx0XHRcdFx0b25DbG9zZS5jYWxsKCk7XHJcblx0XHRcdH0pO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0dGhpcy4kZWwub2ZmKFwiaGlkZGVuLmJzLm1vZGFsXCIpO1xyXG5cdFx0fVxyXG5cclxuXHRcdHRoaXMuJGVsLm1vZGFsKFwic2hvd1wiKTtcclxuXHR9XHJcblxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBIb25leU1lc3NlbmdlcjtcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHNyYy9ub2RlX21vZHVsZXMvaG9uZXlfbWVzc2VuZ2VyL2luZGV4LmpzIiwibW9kdWxlLmV4cG9ydHMgPSBcIjxkaXYgaWQ9XFxcImhvbmV5TW9kYWxcXFwiIGNsYXNzPVxcXCJtb2RhbFxcXCIgdGFiaW5kZXg9XFxcIi0xXFxcIiByb2xlPVxcXCJkaWFsb2dcXFwiPlxcclxcblxcdDxkaXYgY2xhc3M9XFxcIm1vZGFsLWRpYWxvZ1xcXCIgcm9sZT1cXFwiZG9jdW1lbnRcXFwiPlxcclxcblxcdFxcdDxkaXYgY2xhc3M9XFxcIm1vZGFsLWNvbnRlbnRcXFwiPlxcclxcblxcdFxcdFxcdDxkaXYgY2xhc3M9XFxcIm1vZGFsLWhlYWRlclxcXCI+XFxyXFxuXFx0XFx0XFx0XFx0PGg1IGNsYXNzPVxcXCJtb2RhbC10aXRsZVxcXCI+TW9kYWwgdGl0bGU8L2g1PlxcclxcblxcdFxcdFxcdFxcdDxidXR0b24gdHlwZT1cXFwiYnV0dG9uXFxcIiBjbGFzcz1cXFwiY2xvc2VcXFwiIGRhdGEtZGlzbWlzcz1cXFwibW9kYWxcXFwiIGFyaWEtbGFiZWw9XFxcIkNsb3NlXFxcIj5cXHJcXG5cXHRcXHRcXHRcXHRcXHQ8c3BhbiBhcmlhLWhpZGRlbj1cXFwidHJ1ZVxcXCI+JnRpbWVzOzwvc3Bhbj5cXHJcXG5cXHRcXHRcXHRcXHQ8L2J1dHRvbj5cXHJcXG5cXHRcXHRcXHQ8L2Rpdj5cXHJcXG5cXHRcXHRcXHQ8ZGl2IGNsYXNzPVxcXCJtb2RhbC1ib2R5XFxcIj5cXHJcXG5cXHRcXHRcXHRcXHQ8cD5Nb2RhbCBib2R5IHRleHQgZ29lcyBoZXJlLjwvcD5cXHJcXG5cXHRcXHRcXHQ8L2Rpdj5cXHJcXG5cXHRcXHRcXHQ8ZGl2IGNsYXNzPVxcXCJtb2RhbC1mb290ZXJcXFwiPlxcclxcblxcdFxcdFxcdFxcdDxidXR0b24gdHlwZT1cXFwiYnV0dG9uXFxcIiBjbGFzcz1cXFwiYnRuIGJ0bi1ydWJ5XFxcIiBkYXRhLWRpc21pc3M9XFxcIm1vZGFsXFxcIj7Ql9Cw0LrRgNGL0YLRjDwvYnV0dG9uPlxcclxcblxcdFxcdFxcdDwvZGl2PlxcclxcblxcdFxcdDwvZGl2PlxcclxcblxcdDwvZGl2PlxcclxcbjwvZGl2PlwiXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9zcmMvbm9kZV9tb2R1bGVzL2hvbmV5X21lc3Nlbmdlci90ZW1wbGF0ZS5odG1sXG4vLyBtb2R1bGUgaWQgPSAyXG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsIm1vZHVsZS5leHBvcnRzID0gXCI8ZGl2IGNsYXNzPVxcXCJjb21tZW50XFxcIiBkYXRhLWlkPVxcXCI8JS0gaWQgJT5cXFwiPlxcclxcblxcdDxkaXYgY2xhc3M9XFxcInRpdGxlXFxcIj48JS0gbmFtZSAlPjwvZGl2PlxcclxcblxcdDxkaXYgY2xhc3M9XFxcImVtYWlsXFxcIj48YSBocmVmPVxcXCJtYWlsdG86PCUtIGVtYWlsICU+XFxcIj48JS0gZW1haWwgJT48L2E+PC9kaXY+XFxyXFxuXFx0PGRpdiBjbGFzcz1cXFwiYm9keVxcXCI+PCUtIGNvbW1lbnQgJT48L2Rpdj5cXHJcXG48L2Rpdj5cXHJcXG5cIlxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vc3JjL25vZGVfbW9kdWxlcy9ob25leV9jb21tZW50YXRvci9jb21tZW50X3RlbXBsYXRlLmh0bWxcbi8vIG1vZHVsZSBpZCA9IDNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIl0sIm1hcHBpbmdzIjoiOztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBOzs7Ozs7Ozs7Ozs7Ozs7QUM3REE7QUFDQTs7O0FBQUE7QUFDQTs7Ozs7OztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTs7O0FBREE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUNBO0FBS0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUlBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUE1QkE7QUFDQTtBQThCQTtBQUNBO0FBQ0E7QUFFQTtBQUNBOzs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7OztBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFJQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXRCQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBdUJBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQTFCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBNEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXZDQTtBQUNBO0FBeUNBO0FBQ0E7Ozs7OztBQUlBOzs7Ozs7Ozs7Ozs7Ozs7QUNuTUE7QUFDQTs7Ozs7OztBQUNBOzs7Ozs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQUlBOzs7Ozs7QUM3QkE7Ozs7OztBQ0FBOzs7QSIsInNvdXJjZVJvb3QiOiIifQ==