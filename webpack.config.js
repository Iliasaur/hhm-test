"use strict";


const NODE_ENV = process.env.NODE_ENV || "development";
//const API_URI = "http://hhm/api/";
const API_URI = "http://test.webbuild.ru/api/";
const webpack = require("webpack");

module.exports = [

	{ //--- Honey Commentator
		entry: {HoneyCommentator: "./src/node_modules/honey_commentator"},
		
		output: {path: __dirname + "/assets/js", filename: "honey_commentator.js", library: "HoneyCommentator"},

		watch: NODE_ENV == "development",
		
		watchOptions: {aggregateTimeout: 300},
		
		devtool: NODE_ENV == "development" ? "cheap-inline-module-source-map" : false,

		plugins: [new webpack.DefinePlugin({NODE_ENV: JSON.stringify(NODE_ENV), API_URI: JSON.stringify(API_URI)})],

		module: {rules: [{test: /\.js$/, loader: "babel-loader", query: {presets: ["env"]}}, {test: /\.html$/, use: "raw-loader"}]}
	}
]

if (NODE_ENV == "production") {
	module.exports.forEach(function(item, i, arr) {
		item.plugins.push(new webpack.optimize.UglifyJsPlugin({compress: {warnings: false, drop_console: true, unsafe: true}}));
	})
}

