<?php
error_reporting(0);
ini_set("display_errors", 0);



$_POST["userName"] = isset($_POST["userName"]) ? trim($_POST["userName"]) : "";
if (empty($_POST["userName"])) {
  exit(json_encode(array("code" => "ERROR", "message" => "Наберите Ваше имя", "field" => "username"), JSON_UNESCAPED_UNICODE));
}

$_POST["eMail"] = isset($_POST["eMail"]) ? trim($_POST["eMail"]) : "";
if (empty($_POST["eMail"])) {
  exit(json_encode(array("code" => "ERROR", "message" => "Наберите Ваш E-Mail", "field" => "eMail"), JSON_UNESCAPED_UNICODE));
}

if (!filter_var($_POST["eMail"], FILTER_VALIDATE_EMAIL)) {
  exit(json_encode(array("code" => "ERROR", "message" => "Неправильный E-Mail", "field" => "eMail"), JSON_UNESCAPED_UNICODE));
}

$_POST["comment"] = isset($_POST["comment"]) ? trim($_POST["comment"]) : "";
if (empty($_POST["comment"])) {
  exit(json_encode(array("code" => "ERROR", "message" => "Наберите Ваш комментарий", "field" => "comment"), JSON_UNESCAPED_UNICODE));
}

if (($_POST["userName"] != strip_tags($_POST["userName"])) || ($_POST["eMail"] != strip_tags($_POST["eMail"])) || ($_POST["comment"] != strip_tags($_POST["comment"]))) {
  exit(json_encode(array("code" => "ERROR", "message" => "Увы, форматирование не поддерживается. Уберите, пожалуйста, тэги.", "field" => "comment"), JSON_UNESCAPED_UNICODE));
}


require_once($_SERVER["DOCUMENT_ROOT"]."/api/mods/mysql_handler.php");

try {
  $mysql = new MySqlHandler();

  $sql = "insert into ".DB_PREFIX."user_comment (uc_name, uc_email, uc_comment) values (?, ?, ?)";
  $params = [$_POST["userName"], $_POST["eMail"], $_POST["comment"]];
  $mysql->query($sql, $params);
  $ucId = $mysql->lastInsertId();

  $mysql = null;

} catch(Exception $e) {
  exit(json_encode(array("code" => "ERROR", "message" => $e->getMessage()), JSON_UNESCAPED_UNICODE));
}


exit(json_encode(array("code" => "OK", "id" => $ucId), JSON_UNESCAPED_UNICODE));
?>
