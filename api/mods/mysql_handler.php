<?php

class MySqlHandler {

	//------------- Constructor
	function __construct() {
		if (!class_exists("PDO")) {
			throw new Exception("PHP-класс PDO не установлен");
		}

		if (!file_exists($_SERVER["DOCUMENT_ROOT"]."/api/mods/constants.php")) {
			throw new Exception("Не удалось найти файл настроек для подключения к базе данных");
		}

		require_once($_SERVER["DOCUMENT_ROOT"]."/api/mods/constants.php");

		if (!defined("DB_HOST")) {
			throw new Exception("Не удалось получить хост БД");
		}

		if (!defined("DB_NAME")) {
			throw new Exception("Не удалось получить имя БД");
		}

		if (!defined("DB_LOGIN") or !defined("DB_PASSWORD")) {
			throw new Exception("Не удалось получить параметры для подключения БД");
		}

		$this->connection = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_LOGIN, DB_PASSWORD);
		if (!$this->connection) { throw new Exception("Не удалось соединиться с базой данных"); }
		$query = $this->connection->prepare("SET NAMES utf8");
		$query->execute();
		$query = $this->connection->prepare("SET LOCAL time_zone=\"".date("P")."\"");
		$query->execute();
	}


	//------------- Destructor
	function __destruct() {
		$this->connection = null;
	}


	//------------- Query
	function query($sql, $params) {
		$query = $this->connection->prepare($sql);
		$result = $query->execute($params);
		if ($result === false) {
			throw new Exception("Не удалось выполнить SQL-запрос");
		}
		return $query;
	}


	//------------- Last Insert Id
	function lastInsertId() {
		return $this->connection->lastInsertId();
	}
}

?>
