<?php
error_reporting(0);
ini_set("display_errors", 0);


require_once($_SERVER["DOCUMENT_ROOT"]."/api/mods/mysql_handler.php");

try {
	$mysql = new MySqlHandler();

	$comments = [];
	$sql = "select uc_id as id, uc_name as name, uc_email as email, uc_comment as comment from ".DB_PREFIX."user_comment order by uc_id desc";
	$dataset = $mysql->query($sql, null);
	while ($comment = $dataset->fetch(PDO::FETCH_ASSOC)) {
		array_push($comments, $comment);
	}
	$mysql = null;

} catch(Exception $e) {
	exit(json_encode(array("code" => "ERROR", "message" => $e->getMessage()), JSON_UNESCAPED_UNICODE));
}


exit(json_encode(array("code" => "OK", "comments" => $comments), JSON_UNESCAPED_UNICODE));
?>
